import React, {Component} from 'react'

class Timer extends Component{
  constructor(props){
    super(props)
    this.state = {
      time: 0,
      date: new Date()
    }
  }

  componentDidMount(){
    if (this.props.start !== undefined){
      this.setState({time: this.props.start})
    }
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount(){
    clearInterval(this.timerID);
  }

  componentDidUpdate() {
      if (this.state.time === 0) {
          this.componentWillUnmount()
      }
  }

  tick() {
    this.setState({
      time: this.state.time - 1,
      date: new Date()
    });
  }


  render(){
    return(
      <>
        {/* {console.log(this.state.time)} */}
        {this.state.time > 0 && 
            <h3>
                sekarang jam : {this.state.date.toLocaleTimeString()}
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                hitung mundur : {this.state.time}
            </h3>
        }
      </>
    )
  }
}

export default Timer