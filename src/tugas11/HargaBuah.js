import React from 'react';

let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
  ]

class HargaBuah extends React.Component {
    render() {
        return (
            <>
            <h1>Tabel Harga Buah</h1>
            <table border={0} width={700} style={{marginLeft: "auto", marginRight: "auto", border: "1px solid #000"}}>
                <tr style={{backgroundColor: "#AAAAAA"}}>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Berat</th>
                </tr>
            {dataHargaBuah.map(record => {
                return (
                    <tr style={{backgroundColor: "#FF7F4F"}}>
                        <td align={"left"}>{record.nama}</td>
                        <td align={"left"}>{record.harga}</td>
                        <td align={"left"}>{record.berat/1000} kg</td>
                    </tr>
                )
            })}
            </table>
            </>
        )
    }
}

export default HargaBuah