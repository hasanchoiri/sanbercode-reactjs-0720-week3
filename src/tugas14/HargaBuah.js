import React, { useState, useEffect } from 'react';
import axios from 'axios';

const HargaBuah = () => {
    const [dataHargaBuah, setDataHargaBuah] =  useState(null)
    const [inputHargaBuah, setInputHargaBuah]  =  useState("")
    const [indexOfForm, setIndexOfForm] =  useState(-1)

    const handleChange = (event) => {
        let inputHargaBuahNew = {...inputHargaBuah}
        inputHargaBuahNew[event.target.name] = event.target.value

        setInputHargaBuah(inputHargaBuahNew)
    }

    const handleSubmit = (event) => {
        console.log(inputHargaBuah)
        event.preventDefault()
        const index = indexOfForm
        const record = dataHargaBuah[index]
        // console.log(record)

        if (inputHargaBuah.name
            && inputHargaBuah.price
            && inputHargaBuah.weight) {
                if (index === -1) {
                    // new record
                    axios.post('http://backendexample.sanbercloud.com/api/fruits', inputHargaBuah)
                    .then(res => {
                        console.log(res.data)
                        setDataHargaBuah([...dataHargaBuah, res.data])
                    })
                } else {
                    // update record
                    axios.put('http://backendexample.sanbercloud.com/api/fruits/'+ record.id, {...inputHargaBuah})
                    .then(res => {
                        dataHargaBuah[index] = res.data
                        setDataHargaBuah(dataHargaBuah)
                    })
                }        
            }
        // this.resetForm();
    }

    const handleUbah = (event) => {
        // console.log(event.target)
        let index = event.target.value
        let record = dataHargaBuah[index]
        setInputHargaBuah(record)
        setIndexOfForm(index)
    }

    const handleHapus = (event) => {
        let index = event.target.value
        let record = dataHargaBuah[index]

        axios.delete('http://backendexample.sanbercloud.com/api/fruits/'+ record.id)
        .then(res => {
            // console.log(res)
            if (res === "success") {
                
            }
        })
    }
    
    useEffect(()=>{
        if (dataHargaBuah === null) {
            axios.get('http://backendexample.sanbercloud.com/api/fruits')
            .then(res => {
                setDataHargaBuah(
                    res.data.map(el => {
                        return {
                            id: el.id,
                            name: el.name,
                            price: el.price,
                            weight: el.weight
                        }
                    })
                )
            })
        }
    })
    
    return (
        <>
        <h1>List Harga Buah</h1>
        <table border={0} width={700} style={{marginLeft: "auto", marginRight: "auto", border: "1px solid #000"}}>
            <thead>    
                <tr style={{backgroundColor: "#AAAAAA"}}>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Berat</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {/* {console.log(dataHargaBuah)} */}
                {dataHargaBuah !== null && dataHargaBuah.map((item, value) => {
                    // console.log(item)
                    return (
                        <tr style={{backgroundColor: "#FF7F4F"}}>
                            <td align={"left"}>{item.name}</td>
                            <td align={"left"}>{item.price}</td>
                            <td align={"left"}>{item.weight/1000} kg</td>
                            <td>
                                <button onClick={handleUbah} value={value}>Ubah</button>
                                &nbsp;|&nbsp;
                                <button onClick={handleHapus} value={value}>Hapus</button>
                            </td>
                        </tr>
                    )
                })}
                {/* Form */}
                <tr>
                    <td colSpan={4}>
                        <form onSubmit={handleSubmit}>
                            <table width={"100%"}>
                                <thead>
                                    <tr><td colSpan={2}><h1>Form Harga Buah</h1></td></tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Masukkan nama buah:</td>
                                        <td align={"left"}><input type="text" name='name' id={inputHargaBuah.id} value={inputHargaBuah.name} onChange={handleChange}/></td>
                                    </tr>
                                    <tr>
                                        <td>Masukkan harga buah:</td>
                                        <td align={"left"}><input type="text" name='price' id={inputHargaBuah.id} value={inputHargaBuah.price} onChange={handleChange}/></td>
                                    </tr>
                                    <tr>
                                        <td>Masukkan berat buah:</td>
                                        <td align={"left"}><input type="text" name='weight' id={inputHargaBuah.id} value={inputHargaBuah.weight} onChange={handleChange}/></td>
                                    </tr>
                                    <tr><td colSpan={2}>
                                        {/* <input type="hidden" action=/> */}
                                        <button>Submit</button>
                                    </td></tr>
                                </tbody>
                            </table>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
        </>
    )
}

export default HargaBuah