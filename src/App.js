import React from "react"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom"
import './App.css';
import Routes from "./tugas15/Routes";
// import HargaBuah from './tugas11/HargaBuah';
// import Timer from './tugas12/Timer';
// import HargaBuah from './tugas13/HargaBuah';
// import HargaBuah from './tugas14/HargaBuah';
// import HargaBuah from './tugas15/HargaBuah';

function App() {
  return (
    <div className="App" style={{fontFamily: "Times New Normal"}}>
      <Router>
        <Routes />
      </Router>
      {/* tugas 11 */}
      {/* <HargaBuah /> */}
      {/* <br />
      <Timer start={100} /> */}
      {/* <HargaBuah /> */}
    </div>
  );
}

export default App;
