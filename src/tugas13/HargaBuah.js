import React from 'react';

class HargaBuah extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            dataHargaBuah: [
                {nama: "Semangka", harga: 10000, berat: 1000},
                {nama: "Anggur", harga: 40000, berat: 500},
                {nama: "Strawberry", harga: 30000, berat: 400},
                {nama: "Jeruk", harga: 30000, berat: 1000},
                {nama: "Mangga", harga: 30000, berat: 500}
            ],
            inputHargaBuah: {
                nama: '',
                harga: '',
                berat: ''
            },
            action: ''
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleUbah = this.handleUbah.bind(this)
        this.handleHapus = this.handleHapus.bind(this)
    }

    handleChange(event) {
        let inputHargaBuah = {...this.state.inputHargaBuah}
        inputHargaBuah[event.target.name] = event.target.value
        this.setState({
            inputHargaBuah
        })
    }

    handleSubmit(event) {
        event.preventDefault()

        if (this.state.action === "ubah") {
            let index = this.state.dataHargaBuah.findIndex(rec => rec.nama === this.state.inputHargaBuah.nama)
            if (index !== -1) {
                let dataHargaBuah = this.state.dataHargaBuah
                dataHargaBuah[index] = this.state.inputHargaBuah
                this.setState({
                    dataHargaBuah: dataHargaBuah,
                    inputHargaBuah: '',
                    action: ''
                })
            }
        } else {
            this.setState({
                dataHargaBuah: [...this.state.dataHargaBuah, this.state.inputHargaBuah],
                inputHargaBuah: '',
                action: ''
            })   
        }
        // this.resetForm();
    }

    handleUbah(event) {
        // console.log(event.target)
        let value = event.target.value
        let record = this.state.dataHargaBuah[value]
        this.setState({
            inputHargaBuah: {
                ...record
            },
            action: 'ubah'
        })
        // this.resetForm();
    }

    handleHapus(event) {
        let value = event.target.value
        // let record = this.state.dataHargaBuah[value]

        this.state.dataHargaBuah.splice(value, 1)
        this.setState({
            dataHargaBuah: this.state.dataHargaBuah,
            inputHargaBuah: ''
        })
        // this.resetForm();
    }

    // resetForm = () => { 
    //     this.myFormRef.reset();
    // }

    render() {
        return (
            <>
            <h1>Tabel Harga Buah</h1>
            <table border={0} width={700} style={{marginLeft: "auto", marginRight: "auto", border: "1px solid #000"}}>
                <thead>    
                    <tr style={{backgroundColor: "#AAAAAA"}}>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.dataHargaBuah.map((record, value) => {
                        // console.log(value)
                        return (
                            <tr style={{backgroundColor: "#FF7F4F"}}>
                                <td align={"left"}>{record.nama}</td>
                                <td align={"left"}>{record.harga}</td>
                                <td align={"left"}>{record.berat/1000} kg</td>
                                <td>
                                    <button onClick={this.handleUbah} value={value}>Ubah</button>
                                    &nbsp;|&nbsp;
                                    <button onClick={this.handleHapus} value={value}>Hapus</button>
                                </td>
                            </tr>
                        )
                    })}
                    {/* Form */}
                    <tr>
                        <td colSpan={4}>
                            <form onSubmit={this.handleSubmit}>
                                <table width={"100%"}>
                                    <thead>
                                        <tr><td colSpan={2}><h1>Form Peserta</h1></td></tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Masukkan nama buah:</td>
                                            <td align={"left"}><input type="text" name='nama' value={this.state.inputHargaBuah.nama} onChange={this.handleChange}/></td>
                                        </tr>
                                        <tr>
                                            <td>Masukkan harga buah:</td>
                                            <td align={"left"}><input type="text" name='harga' value={this.state.inputHargaBuah.harga} onChange={this.handleChange}/></td>
                                        </tr>
                                        <tr>
                                            <td>Masukkan berat buah:</td>
                                            <td align={"left"}><input type="text" name='berat' value={this.state.inputHargaBuah.berat} onChange={this.handleChange}/></td>
                                        </tr>
                                        <tr><td colSpan={2}>
                                            {/* <input type="hidden" action=/> */}
                                            <button>Submit</button>
                                        </td></tr>
                                    </tbody>
                                </table>
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
            </>
        )
    }
}

export default HargaBuah