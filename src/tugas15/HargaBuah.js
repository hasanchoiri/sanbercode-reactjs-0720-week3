import React from "react"
import "./HargaBuah.css"
import { HargaBuahProvider } from "./HargaBuahContext"
import HargaBuahList from "./HargaBuahList"
import HargaBuahForm from "./HargaBuahForm"

const HargaBuah = () => {
    return (
        <HargaBuahProvider>
            <HargaBuahList />
            <HargaBuahForm />
        </HargaBuahProvider>
    )
}

export default HargaBuah