import React, {useContext, useEffect} from "react"
import { HargaBuahContext } from "./HargaBuahContext"
import axios from 'axios';

const HargaBuahForm = () => {
    const [hargaBuah, setHargaBuah, input, setInput, selectedId, setSelectedId, statusForm, setStatusForm] = useContext(HargaBuahContext)

    const handleChange = (event) => {
        let newHargaBuah = {...input}
        newHargaBuah[event.target.name] = event.target.value

        setInput(newHargaBuah)
    }

    const handleSubmit = (event) => {
        event.preventDefault()

        let name = input.name
        let price = input.price.toString()
        let weight = input.weight.toString()

        if (name.replace(/\s/g,'') !== ""
            && price.replace(/\s/g,'') !== ""
            && weight.replace(/\s/g,'') !== "") {
                if (statusForm === "create") {
                    // new record
                    axios.post(`http://backendexample.sanbercloud.com/api/fruits`, input)
                    .then(res => {
                        console.log(res.data)
                        setHargaBuah([...hargaBuah, {
                            id: res.data.id,
                            name: input.name,
                            price: input.price,
                            weight: input.weight
                        }])
                    })
                } else if (statusForm === "edit") {
                    // update record
                    axios.put(`http://backendexample.sanbercloud.com/api/fruits/${selectedId}`, {...input})
                    .then(res => {
                        let dataBuah = hargaBuah.find(el=> el.id === selectedId)
                        dataBuah.name = input.name
                        dataBuah.price = input.price
                        dataBuah.weight = input.weight
                        setHargaBuah([...hargaBuah])
                    })
                }

                setStatusForm("create")
                setSelectedId(0)
                setInput({name: "", price: "", weight: 0})
            }
    }

    useEffect( () => {
        if (hargaBuah === null){
            axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
            .then(res => {
                console.log(res)
                setHargaBuah(res.data.map(el=>{ return {id: el.id, name: el.name, price: el.price, weight: el.weight }} ))
            })
        }
    }, [hargaBuah])


    return(
        <>
        {/* <ul>
            <h1>Daftar Harga Buah</h1>
            <table>
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Berat</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                    {
                    hargaBuah !== null && hargaBuah.map((item, index)=>{
                        return(                    
                        <tr key={index}>
                            <td>{index+1}</td>
                            <td>{item.name}</td>
                            <td>{item.price}</td>
                            <td>{item.weight/1000} kg</td>
                            <td>
                            <button onClick={handleEdit} value={item.id}>Edit</button>
                            &nbsp;
                            <button onClick={handleDelete} value={item.id}>Delete</button>
                            </td>
                        </tr>
                        )
                    })
                    }
                </tbody>
            </table>
        </ul> */}
          {/* Form */}
          <h1>Form Daftar Harga Buah</h1>
    
          <div style={{width: "50%", margin: "0 auto", display: "block"}}>
            <div style={{border: "1px solid #aaa", padding: "20px"}}>
              <form onSubmit={handleSubmit}>
                <label style={{float: "left"}}>
                  Nama:
                </label>
                <input style={{float: "right"}} type="text" name="name" value={input.name} onChange={handleChange}/>
                <br/>
                <br/>
                <label style={{float: "left"}}>
                  Harga:
                </label>
                <input style={{float: "right"}} type="text" name="price" value={input.price} onChange={handleChange}/>
                <br/>
                <br/>
                <label style={{float: "left"}}>
                  Berat (dalam gram):
                </label>
                <input style={{float: "right"}} type="number" name="weight" value={input.weight} onChange={handleChange}/>
                <br/>
                <br/>
                <div style={{width: "100%", paddingBottom: "20px"}}>
                  <button style={{ float: "right"}}>Submit</button>
                </div>
              </form>
            </div>
          </div>
        </>
    )    
}

export default HargaBuahForm