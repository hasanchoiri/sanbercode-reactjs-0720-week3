import React, {useContext, useState, useEffect} from "react"
import { HargaBuahContext } from "./HargaBuahContext"
import axios from 'axios';

const HargaBuahList = () => {
    const [hargaBuah, setHargaBuah, input, setInput, selectedId, setSelectedId, statusForm, setStatusForm] = useContext(HargaBuahContext)

    const handleEdit = (event) => {
        // console.log(event)
        let idDataBuah = parseInt(event.target.value)
        let dataBuah = hargaBuah.find(x => x.id === idDataBuah)
        setInput({
            name: dataBuah.name,
            price: dataBuah.price,
            weight: dataBuah.weight
        })
        setSelectedId(idDataBuah)
        setStatusForm("edit")
    }

    const handleDelete = (event) => {
        // console.log(event)
        let idDataBuah = parseInt(event.target.value)
        let id = event.target.value
        let allRecord = hargaBuah.filter(el => el.id !== idDataBuah)

        axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
        .then(res => {
            // console.log(res)
        })
            
        setHargaBuah([...allRecord])
    }

    return(
        <ul>
            <h1>Daftar Harga Buah</h1>
            <table>
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Berat</th>
                    <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                    {
                    hargaBuah !== null && hargaBuah.map((item, index)=>{
                        return(                    
                        <tr key={index}>
                            <td>{index+1}</td>
                            <td>{item.name}</td>
                            <td>{item.price}</td>
                            <td>{item.weight/1000} kg</td>
                            <td>
                            <button onClick={handleEdit} value={item.id}>Edit</button>
                            &nbsp;
                            <button onClick={handleDelete} value={item.id}>Delete</button>
                            </td>
                        </tr>
                        )
                    })
                    }
                </tbody>
            </table>
        </ul>
    )
}

export default HargaBuahList