import React from "react";
import { Switch, Link, Route } from "react-router-dom";

import UserInfo from '../tugas11/HargaBuah';
import Timer from '../tugas12/Timer';
import Lists from '../tugas13/HargaBuah';
import HooksLists from '../tugas14/HargaBuah';
import ContextLists from '../tugas15/HargaBuah';


const Routes = () => {
    return (
        <>
          <nav>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/timer">Timer</Link>
              </li>
              <li>
                <Link to="/lists">Lists dengan class</Link>
              </li>
              <li>
                <Link to="/hook-lists">Lists dengan Hooks</Link>
              </li>
              <li>
                <Link to="/simple-hooks">Simple Hooks</Link>
              </li>
              <li>
                <Link to="/lists-context">Lists dengan context</Link>
              </li>
    
            </ul>
          </nav>
          <Switch>
            <Route path="/timer">
                <Timer start={100}/>
            </Route>
            <Route path="/lists-context" component={ContextLists}/>
            <Route path="/lists" component={Lists}/>
            <Route path="/hook-lists" component={HooksLists}/>
            <Route path="/" component={UserInfo}/>
          </Switch>
        </>
      )    
}

export default Routes;