import React, { useState, createContext } from "react";

export const HargaBuahContext = createContext()
export const HargaBuahProvider = props => {
    const [hargaBuah, setHargaBuah] = useState(null)
    const [input, setInput]  =  useState({name: "", price: 0, weight: 0})
    const [selectedId, setSelectedId]  =  useState(0)
    const [statusForm, setStatusForm]  =  useState("create")

    return (
        <HargaBuahContext.Provider value={[hargaBuah, setHargaBuah, input, setInput, selectedId, setSelectedId, statusForm, setStatusForm]}>
            {props.children}
        </HargaBuahContext.Provider>
    )
}